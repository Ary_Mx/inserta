﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public AudioSource click;
    public ElementoInteractivo comenzar;

	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Salir();
        if (comenzar.pulsado)
        {
            LoadNextLevel();
        }
    }

    public void LoadNextLevel()
    {
        click.Play();
        SceneManager.LoadScene("Principal");
    }

    public void Salir()
    {
        click.Play();
        Debug.Log("Salir");
        Application.Quit();
    }

    public void LoadReglasScene()
    {
        click.Play();
        SceneManager.LoadScene("Reglas");
    }

    public void Deserializar()
    {
        Debug.Log("asdf");
    }
}
