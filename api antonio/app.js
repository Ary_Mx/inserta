
const mongoose   = require('mongoose')
const Schema     = mongoose.Schema

const bodyParser = require("body-parser");
const express    = require('express');
const app        = express();

const config  = require('./config');
const points  = require('./model_puntuacion.js');


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())


const user_schema = new Schema({
    username       : String,
    puntos         : { type: Number, default:0},
    fecha_creacion : { type: Date,   default: Date.now() },
    fecha_ultima   : { type: Date,   default: Date.now() }
})
const userModel = mongoose.model('User', user_schema);
const User      = mongoose.model('User');

app.post('/post', function(req, res) {

    User.findOne(
        {"username":req.body.username},
        (err, adventure) => {
            console.log("adventure =" + adventure);
            if (adventure != null) {
                console.log("Updating...");
                User.findOneAndUpdate({_id :adventure.id}, {$inc : {'puntos'       : req.body.puntos}}).exec();
                // User.findOneAndUpdate({_id :adventure.id}, {$set : {'fecha_ultima' : new Date().toISOString()}}).exec();
                User.findOneAndUpdate({_id :adventure.id}, {$set : {'fecha_ultima' : Date.now()}}).exec();
                res.status(200).send({status:"ok"});
            }
            else {
                console.log("Creating...");
                var newUser  = new User({"username":req.body.username, "puntos":req.body.puntos});
                newUser.save(function(error, user){
                    if (error) res.status(500).send({status:"no"});
                    res.status(200).send({status:"ok"});
                })
            }
        }
    );

});






module.exports = app







//
