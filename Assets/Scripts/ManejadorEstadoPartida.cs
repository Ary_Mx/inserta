﻿using System;
using System.IO;
using UnityEngine;

public class ManejadorEstadoPartida : MonoBehaviour
{
    private const string RUTA_ESTADO_PARTIDA = "EstadoPartida.json";

  public int Count;//es para resolver el problema de la función para calcular puntos.
  private static InformacionPartida informacionPartida = new InformacionPartida();// si esto en publico no podemos guardar
    public void RestablecerPartida()
    {
        print("hola auqi");
        StreamReader reader = new StreamReader(RUTA_ESTADO_PARTIDA);
        string informacionPartidaJson = reader.ReadToEnd();
        informacionPartida = JsonUtility.FromJson<InformacionPartida>(informacionPartidaJson);
        print(informacionPartida);
        reader.Close();
    }

    public void GuardarPartida()
    {
        print("estoy guardando la partida");
        StreamWriter writer = new StreamWriter(RUTA_ESTADO_PARTIDA, false);
        writer.Write(JsonUtility.ToJson(informacionPartida));
        writer.Close();
       
    }
    public void Borrar_puntuación()
    {
       informacionPartida.Puntos=0;
        GuardarPartida();
    }
    public string CombinacionActual
    {
        get
        {
            return informacionPartida.CombinacionActual;
        }
        set
        {
            informacionPartida.CombinacionActual = value;
            informacionPartida.PalabrasIntroducidasEnCombinacionActual.Clear();
            informacionPartida.CombinacionesUsadas.Add(value);
        }
    }

    public bool EstaLaPalabraUtilizada(string palabra)
    {
        return informacionPartida.PalabrasIntroducidasEnCombinacionActual.Contains(palabra);
    }
    static bool tutorial=false;
    public void tutorial_activado()
    {
        tutorial = true;
    }
    public void IncrementarPuntos(int incremento)
    {
        informacionPartida.Puntos += incremento;
  
    }

    public int GetPuntos()
    {
        return informacionPartida.Puntos;
    }

    public void AddPalabraUsadaEnCombinacionActual(string palabra)
    {
        informacionPartida.PalabrasIntroducidasEnCombinacionActual.Add(palabra);
    }

    public bool HaSidoCombinacionUsadaYa(string combinacion)
    {
        return informacionPartida.CombinacionesUsadas.Contains(combinacion);
    }

    public int NumeroPalabrasUsadaEnCombinacionActual
    {
        get
        {
            Count = informacionPartida.PalabrasIntroducidasEnCombinacionActual.Count;
            return Count;
            //return informacionPartida.PalabrasIntroducidasEnCombinacionActual.Count;
        }
    }

}
