﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teclado_letra : MonoBehaviour
{
    // Código para ser llamado en el evento de click de cada tecla
    public Text texto_a_modificar;
    public Text mensaje;

    public void Escribir(string caracter)
    {
        texto_a_modificar.text += caracter;
        if (mensaje != null) { mensaje.enabled = false; }
    }
}
