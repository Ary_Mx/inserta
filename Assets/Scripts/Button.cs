﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public AudioSource click;
    public GameObject panelMainMenu;
    public GameObject panelCreditos;

	void Update ()
    {       
    }

    public void LoadNextLevel()
    {
        click.Play();
        SceneManager.LoadScene("Principal");
    }
    public void LoadPreviousLevel()
    {
        click.Play();
        SceneManager.LoadScene("Inicio");
    }
    public void Salir()
    {
        click.Play();
        Debug.Log("Salir");
        Application.Quit();
    }

    public void LoadReglasScene()
    {
        click.Play();
        SceneManager.LoadScene("Reglas");
    }

    public void Deserializar()
    {
        Debug.Log("asdf");
    }
    public void CambiarPaneles(int n)
    {
        if(n == 0){
            panelMainMenu.SetActive(true);
            panelCreditos.SetActive(false);
        }else{
            panelMainMenu.SetActive(false);
            panelCreditos.SetActive(true);
        }
    }
    public void ClickSound(){
        click.Play();
    }
}
