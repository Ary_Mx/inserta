﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//
public class Textos : MonoBehaviour
{
    public static bool tutorial;
    public Text combinacion;
    public Text puntos;
    public Text numeroPalabrasPorCombinacionText;
    public Text mensaje;
    public BBDD baseDeDatos;
    public InputField IntroducePalabra;
    public Text inputText;
    public AudioSource validacion;
    public AudioSource error;
    public AudioSource click;
    private int numeroPalabrasDisponiblesEnCombinacion;
    public ManejadorEstadoPartida manejadorEstadoPartida;
    public Text inteligenciaFallo;
    public Text pista;
    private int fallosSeguidos = 0;
    private int aciertosSeguidos = 0;
    private int faseTutorial;
    public int minimoPuntos, maximoPuntos;
    private Dictionary<string, string> wordLetter = new Dictionary<string, string>();
    public List<GameObject> pantallasTutorial = new List<GameObject>();
    List<char> firstCom;
    string combinacionVocal;

    // Para calcular los puntos 5 , 10,15 ,20
     static int incrementador_5_puntos;
    private int NumeroPalabrasDisponiblesEnCombinacion
    {
        get
        {
            return numeroPalabrasDisponiblesEnCombinacion;
        }

        set
        {
            numeroPalabrasDisponiblesEnCombinacion = value;
            numeroPalabrasPorCombinacionText.text = "Palabras disponibles para esta combinación: " + numeroPalabrasDisponiblesEnCombinacion;
        }
    }
    public void Tutorial(bool set)
    {
        tutorial = set;
    }
    public void Tutorial_off()
    {
        tutorial = false;
    }
    public void Start()
    {
        // Create a temporary reference to the current scene.
        Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        string sceneName = currentScene.name;
        if (sceneName=="Principal")
        {
            if (PlayerPrefs.GetInt("veterano") == 0)
            {
                tutorial = true;
                PlayerPrefs.SetInt("veterano", 1);
            }
            //if (manejadorEstadoPartida.CombinacionActual == null)
            ObtenerCombinacion();
            //else
            //InicializarInterfazConCombinacion(manejadorEstadoPartida.CombinacionActual);
            puntos.text = manejadorEstadoPartida.GetPuntos().ToString();
            mensaje.enabled = false;
            IntroducePalabra.ActivateInputField();
            if (tutorial)
            {
                manejadorEstadoPartida.tutorial_activado();
                manejadorEstadoPartida.IncrementarPuntos(45);
                pantallasTutorial[faseTutorial].SetActive(true);
            }
        }
       
    }

    public void Update()
    {
       // print(incrementador_5_puntos);
        // Create a temporary reference to the current scene.
        Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        string sceneName = currentScene.name;
        if (sceneName == "Principal")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                
                click.Play();
                SceneManager.LoadScene("Inicio");
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Validar();
            }
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                ObtenerCombinacion();
            }
        }
        if (sceneName == "Inicio")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

    }

    public void ObtenerCombinacion()
    {
        click.Play();
        incrementador_5_puntos = 0;//Para calcular los puntos 
        List<string> combinaciones = baseDeDatos.ExtraerCombinaciones();
        string combinacionActual = "";
        mensaje.text = "";
        pista.text = "";

        if (tutorial && faseTutorial < 2)
        {
            combinacionActual = "CS";
          //  Debug.Log("primer caso");
        }
        else
        {
            //Debug.Log("segundo caso");
            do
            {
                int indiceCombinacion = Random.Range(0,combinaciones.Count);
                combinacionActual = combinaciones.ElementAt(indiceCombinacion);
            } while (manejadorEstadoPartida.HaSidoCombinacionUsadaYa(combinacionActual));
        }
        inputText.text = null;
        manejadorEstadoPartida.CombinacionActual = combinacionActual;
        InicializarInterfazConCombinacion(combinacionActual);
    }

    private void InicializarInterfazConCombinacion(string combinacionString)
    {
        IntroducePalabra.ActivateInputField();
        combinacion.text = combinacionString;
        NumeroPalabrasDisponiblesEnCombinacion = baseDeDatos.ExtraerPalabrasParaCombinacion(combinacionString) - manejadorEstadoPartida.NumeroPalabrasUsadaEnCombinacionActual;
    }

    public void Validar()
    {
        string palabra = inputText.text.ToUpper();
        if (tutorial)
        {
            IncrementarTutorial();
        }

        if (manejadorEstadoPartida.EstaLaPalabraUtilizada(palabra))
        {
            error.Play();
            IntroducePalabra.ActivateInputField();
            mensaje.text = "La palabra \"" + palabra.ToLower() + "\" ya ha sido introducida";
            mensaje.enabled = true;
            IntroducePalabra.text = "";
            puntos.text = manejadorEstadoPartida.GetPuntos().ToString();
            RestablecerMensaje();
            inputText.text = null;
            MostrarMensaje();
            return;
        }

        int puntosDePalabra = baseDeDatos.ExtraerPuntos(manejadorEstadoPartida.CombinacionActual, palabra);
        if (puntosDePalabra == 0)
        {
            IntroducePalabra.ActivateInputField();
            error.Play();
            mensaje.text = "La palabra NO se encuentra en la base de datos";
            mensaje.enabled = true;
            IntroducePalabra.text = "";
            fallosSeguidos++;
            aciertosSeguidos = 0;
        }
        else
        {
            IntroducePalabra.ActivateInputField();
            //  manejadorEstadoPartida.IncrementarPuntos(CalcularPuntos());
            manejadorEstadoPartida.IncrementarPuntos(CalcularPuntos_por_numero_de_combinación());
            //  manejadorEstadoPartida.IncrementarPuntos(10);//para solo obtener los puntos el problema de un private y un public
            if (!tutorial)
            {
                manejadorEstadoPartida.GuardarPartida();
            }         
            combinacion.text = manejadorEstadoPartida.CombinacionActual;
            validacion.Play();
            IntroducePalabra.text = "";
            manejadorEstadoPartida.AddPalabraUsadaEnCombinacionActual(palabra);
            NumeroPalabrasDisponiblesEnCombinacion--;
            aciertosSeguidos++;
            fallosSeguidos = 0;
            if (NumeroPalabrasDisponiblesEnCombinacion == 0)
                ObtenerCombinacion();
            print("Numero de combinaciones =0 ");
        }
        puntos.text = manejadorEstadoPartida.GetPuntos().ToString();
        RestablecerMensaje();
        inputText.text = null;
        MostrarMensaje();
    }

    public void PedirPista()
    {
        if (manejadorEstadoPartida.GetPuntos() > 49)
        {
            var vocales = new List<char>() { 'A', 'E', 'I', 'O', 'U', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ü' };
            int pos = 0;

            foreach (string words in baseDeDatos.ExtraerPalabrasCombinacion(manejadorEstadoPartida.CombinacionActual))
            {
                if (!manejadorEstadoPartida.EstaLaPalabraUtilizada(words))
                {
                    if (!wordLetter.ContainsKey(words))
                    {
                        wordLetter.Add(words, "");
                        pista.text = null;
                    }
                    foreach (char vocal in words.ToUpper())
                    {

                        if (vocales.Contains(vocal) && !wordLetter[words].Contains(vocal + pos.ToString()))
                        {
                            //pista.text = null;
                            //pista.text = "La palabra contiene " + vocal + " en la posición " + pos;
                            wordLetter[words] += (vocal + pos.ToString());
                            manejadorEstadoPartida.IncrementarPuntos(-50);
                            Debug.Log("Restar");
                            puntos.text = manejadorEstadoPartida.GetPuntos().ToString();
                            List<char> firstCom = combinacion.text.ToList();
                            firstCom.Insert(pos,vocal);
                            string newCombinacion = null;
                            foreach(var newcomb in firstCom){
                                newCombinacion += newcomb;
                            }
                            combinacion.text = newCombinacion;
                            break;
                        }
                        pos++;
                    }
                    break;
                }
            }
        }else{
            inteligenciaFallo.text = "No dispones de puntos";
        }

    }
    public void RestablecerMensaje()
    {
        if (IntroducePalabra.text != "")
           mensaje.enabled = false;
    }

    public void VolverAlInicio()
    {
        SceneManager.LoadScene("Inicio");
    }

    public void GuardarPartida()
    {
        manejadorEstadoPartida.GuardarPartida();
    }
    private void MostrarMensaje()
    {
        if (fallosSeguidos != 0)
        {
            switch (fallosSeguidos)
            {
                case 5:
                    inteligenciaFallo.text = "fallo 1";
                    break;
                case 10:
                    inteligenciaFallo.text = "fallo 2";
                    break;
                case 15:
                    inteligenciaFallo.text = "fallo 3";
                    break;
                case 20:
                    inteligenciaFallo.text = "fallo 4";
                    break;
                case 25:
                    inteligenciaFallo.text = "fallo 5";
                    break;
                case 30:
                    inteligenciaFallo.text = "fallo 6";
                    fallosSeguidos = 0;
                    break;
                default:
                    inteligenciaFallo.text = "";
                    break;
            }
        }
        else if (aciertosSeguidos != 0)
        {
            switch (aciertosSeguidos)
            {
                case 5:
                    inteligenciaFallo.text = "Vamos !";
                    break;
                case 10:
                    inteligenciaFallo.text = "Sigue así, vas muy bien !";
                    break;
                case 15:
                    inteligenciaFallo.text = "Asombroso !";
                    break;
                case 20:
                    inteligenciaFallo.text = "Qué maravilla !";
                    break;
                case 25:
                    inteligenciaFallo.text = "Madre mía, eres todo un experto !";
                    break;
                case 30:
                    inteligenciaFallo.text = "Voy a hacer un altar dedicado a tí !";
                    aciertosSeguidos = 0;
                    break;
                default:
                    inteligenciaFallo.text = "";
                    break;
            }
        }
        else
        {
            inteligenciaFallo.text = "";
        }
    }
    public int CalcularPuntos_por_numero_de_combinación()
    {
       incrementador_5_puntos=incrementador_5_puntos+5;
     return incrementador_5_puntos;
    }
   public int CalcularPuntos()
   {
        //   float puntos = minimoPuntos + ((float)manejadorEstadoPartida.informacionPartida.PalabrasIntroducidasEnCombinacionActual.Count /
        float puntos = minimoPuntos + ((float)manejadorEstadoPartida.Count /
              (float)baseDeDatos.ExtraerPalabrasParaCombinacion(manejadorEstadoPartida.CombinacionActual)) * ((float)maximoPuntos - (float)minimoPuntos);
        //   if (manejadorEstadoPartida.informacionPartida.PalabrasIntroducidasEnCombinacionActual.Count ==
        if (manejadorEstadoPartida.Count ==
            baseDeDatos.ExtraerPalabrasParaCombinacion(manejadorEstadoPartida.CombinacionActual) - 1)
       {
            puntos *= 2;
      }
        return (int)puntos;
    }
    public void IncrementarTutorial()
    {
        pantallasTutorial[faseTutorial].SetActive(false);
        faseTutorial++;
        if (faseTutorial < pantallasTutorial.Count)
        {
            pantallasTutorial[faseTutorial].SetActive(true);
        }
        else
        {
            tutorial = false;
        }
    }
}
