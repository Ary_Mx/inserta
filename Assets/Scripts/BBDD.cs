﻿using UnityEngine;
using UnityEngine.UI;
using System.Data;
using Mono.Data.SqliteClient;
using System.Collections.Generic;
using System.IO;
using System;

public class BBDD : MonoBehaviour
{
    public Textos textos;
    IDbConnection conexion;

    // Accedemos al archivo de la base de datos de carpeta StreamingAssets al comienzo del código
    public void Start()
    {
        string databaseName = "Combinaciones.db";
        string filepath     = Application.streamingAssetsPath + "/" + databaseName;
        //string filepath     = Application.persistentDataPath + "/" + databaseName;

        if (!File.Exists(filepath))
        {
            WWW loadDB = new WWW(Application.streamingAssetsPath + "/" + databaseName);
            while (!loadDB.isDone) {}
            File.WriteAllBytes(filepath, loadDB.bytes);
        }

        conexion            = new SqliteConnection("URI=file:" + filepath);

        conexion.Open();
    }

    // Extraemos la lista de palabras de la base de datos
    public List<string> ExtraerCombinaciones()
    {
        conexion.Open();

        IDbCommand cmd      = conexion.CreateCommand();
        cmd.CommandText     = "SELECT combination FROM words";
        IDataReader datos   = cmd.ExecuteReader();
        List<string> result = new List<string>();

        while (datos.Read())
        {
          //  Debug.Log(datos.GetName(0));
            result.Add(datos.GetString(0));
        }
        conexion.Close();
        return result;
    }

    // Dada una combinación se cuenta la cantidad de combinaciones posibles
    public int ExtraerPalabrasParaCombinacion(string combinacion)
    {
        conexion.Open();

        IDbCommand cmd    = conexion.CreateCommand();
        cmd.CommandText   = "SELECT COUNT(*) FROM words WHERE combination = '" + combinacion + "'";
        IDataReader datos = cmd.ExecuteReader();

        int cuenta = 0;
        while (datos.Read())
        {
            cuenta = datos.GetInt32(0);
          //  Debug.Log("Cuenta: " + cuenta);
        }
        conexion.Close();
        return cuenta;
    }

    // No está en uso actualmente
    public int ExtraerPuntos (string combinacion, string palabra)
    {
        conexion.Open();

        IDbCommand cmd = conexion.CreateCommand();
        cmd.CommandText = "SELECT points FROM words WHERE combination = '" + combinacion + "' AND word = '" + palabra + "'";
        IDataReader datos = cmd.ExecuteReader();

        int puntos = 0;
        while (datos.Read())
        {
            puntos = datos.GetInt32(0);
            Debug.Log("Puntos: " + puntos);
        }
        conexion.Close();
        return puntos;
    }

    // Extrae una combinación dada una palabra de entrada
    public List<string> ExtraerPalabrasCombinacion(string palabraCombinacion)
    {
        conexion.Open();

        IDbCommand cmd = conexion.CreateCommand();
        cmd.CommandText = "SELECT word FROM words WHERE combination = '" + palabraCombinacion + "'";
        IDataReader datos = cmd.ExecuteReader();

        List<string> result = new List<string>();
        while (datos.Read())
        {
            result.Add(datos.GetString(0));
        }
        conexion.Close();
        return result;
    }
}
