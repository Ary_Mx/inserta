﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fullscreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (Screen.fullScreen)
        {
            Screen.fullScreen = true;
        }
        if (!Screen.fullScreen)
        {
            Screen.fullScreen = true;
        }
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
