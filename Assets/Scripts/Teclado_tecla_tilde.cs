﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// Esta clase se encarga de añadir texto con tilde cuando se mantiene presionada una tecla
public class Teclado_tecla_tilde : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Text texto_a_modificar;
    public CanvasGroup canvas_group_tecla, canvas_group_tecla_2;
    public float contador_tiempo_tecla = 0;
    public bool buttonPressed;

    public string caracter_normal   = "";
    public string caracter_tilde    = "";
    public string caracter_tilde_2  = "";

    Vector2 initialPos;

    float timeToDissapear = 0.6f;

    // Start is called before the first frame update
    void Start()
    {
        canvas_group_tecla.alpha = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        //Parece que no es necesario 
        if(canvas_group_tecla_2 != null)
        {
            timeToDissapear = 1.5f;
            
        }
        if (buttonPressed)
        {
            contador_tiempo_tecla += Time.deltaTime;

            contador_tiempo_tecla  = Mathf.Clamp(contador_tiempo_tecla, 0, timeToDissapear);
        }
        else
        {
            contador_tiempo_tecla -= Time.deltaTime;
            contador_tiempo_tecla  = Mathf.Clamp(contador_tiempo_tecla, 0, timeToDissapear);
        }
        if (contador_tiempo_tecla >= 0.3f)
        {
            canvas_group_tecla.alpha = 1f;
            if(canvas_group_tecla_2 != null)
            {
                canvas_group_tecla_2.alpha = 1f;
            }
        }
        else
        {
            canvas_group_tecla.alpha = 0f;
            if(canvas_group_tecla_2 != null)
            {
                canvas_group_tecla_2.alpha = 0f;
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonPressed = true;
        initialPos = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        buttonPressed = false;
        if (contador_tiempo_tecla >= 0.3f)
        {
            if(eventData.position.x >= initialPos.x && caracter_tilde_2 != "")
            {
                texto_a_modificar.text += caracter_tilde_2;
            }
            else
            {
                texto_a_modificar.text += caracter_tilde;
            }
        }
        else
        {
            texto_a_modificar.text += caracter_normal;
        }
    }
}
