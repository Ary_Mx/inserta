﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Textos: MonoBehaviour
{
    public Text combinacion;
    public Text puntos;
    public Text numeroPalabrasPorCombinacionText;
    public Text mensaje;
    public BBDD baseDeDatos;
    public InputField IntroducePalabra;
    public AudioSource validacion;
    public AudioSource error;
    public AudioSource click;
    private int numeroPalabrasDisponiblesEnCombinacion;
    public ManejadorEstadoPartida manejadorEstadoPartida;

    private int NumeroPalabrasDisponiblesEnCombinacion
    {
        get
        {
            return numeroPalabrasDisponiblesEnCombinacion;
        }

        set
        {
            numeroPalabrasDisponiblesEnCombinacion = value;
            numeroPalabrasPorCombinacionText.text = "Palabras disponibles para esta combinación: " + numeroPalabrasDisponiblesEnCombinacion;
        }
    }

    public void Start()
    {
        if (manejadorEstadoPartida.CombinacionActual == null)
            ObtenerCombinacion();
        else
            InicializarInterfazConCombinacion(manejadorEstadoPartida.CombinacionActual);
        puntos.text = "Puntos: " + manejadorEstadoPartida.GetPuntos();
        mensaje.enabled = false;
        IntroducePalabra.ActivateInputField();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Salir");
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Validar();
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            ObtenerCombinacion();
        }
    }

    public void ObtenerCombinacion()
    {
        click.Play();

        List<string> combinaciones = baseDeDatos.ExtraerCombinaciones();
        string combinacionActual;
        do
        {
            int indiceCombinacion = Random.Range(0, combinaciones.Count);
            combinacionActual = combinaciones.ElementAt(indiceCombinacion);
        } while (manejadorEstadoPartida.HaSidoCombinacionUsadaYa(combinacionActual));

        manejadorEstadoPartida.CombinacionActual = combinacionActual;
        InicializarInterfazConCombinacion(combinacionActual);
    }

    private void InicializarInterfazConCombinacion(string combinacionString)
    {
        IntroducePalabra.ActivateInputField();
        combinacion.text = "Combinación: " + combinacionString;
        NumeroPalabrasDisponiblesEnCombinacion = baseDeDatos.ExtraerPalabrasParaCombinacion(combinacionString) - manejadorEstadoPartida.NumeroPalabrasUsadaEnCombinacionActual;
    }

    public void Validar()
    {
        string palabra = IntroducePalabra.text.ToUpper();
        if (manejadorEstadoPartida.EstaLaPalabraUtilizada(palabra))
        {
            error.Play();
            IntroducePalabra.ActivateInputField();
            mensaje.text = "La palabra \"" + palabra.ToLower() + "\" ya ha sido introducida";
            mensaje.enabled = true;
            IntroducePalabra.text = "";
            return;
        }

        int puntosDePalabra = baseDeDatos.ExtraerPuntos(manejadorEstadoPartida.CombinacionActual, palabra);
        if (puntosDePalabra == 0)
        {
            IntroducePalabra.ActivateInputField();
            error.Play();
            mensaje.text = "La palabra NO se encuentra en la base de datos";
            mensaje.enabled = true;
            IntroducePalabra.text = "";
        }
        else
        {
            IntroducePalabra.ActivateInputField();
            manejadorEstadoPartida.IncrementarPuntos(puntosDePalabra);
            puntos.text = "Puntos: " + manejadorEstadoPartida.GetPuntos();
            validacion.Play();
            IntroducePalabra.text = "";
            manejadorEstadoPartida.AddPalabraUsadaEnCombinacionActual(palabra);
            NumeroPalabrasDisponiblesEnCombinacion--;
            if (NumeroPalabrasDisponiblesEnCombinacion == 0)
                ObtenerCombinacion();
        }
    }

    public void RestablecerMensaje()
    {
        if (IntroducePalabra.text != "")
            mensaje.enabled = false;
    }

    public void VolverAlInicio()
    {
        SceneManager.LoadScene("Inicio");
    }

    public void GuardarPartida()
    {
        manejadorEstadoPartida.GuardarPartida();
    }
}
